const sequelize = require('./sequelize');

async function reset() {
	try {
		await sequelize.authenticate();
		console.log("Database connection OK!");
		console.log("Rewriting database...");
		await sequelize.sync({ force: true });
		await sequelize.models.Category.bulkCreate([
			{
				id: 0,
				name: "Mundo"
			}, {
				id: 1,
				name: "Politica"
			}, {
				id: 2,
				name: "Cine/Entretenimiento"
			}, {
				id: 3,
				name: "Deportes"
			}
		]);
		await sequelize.models.Post.bulkCreate([
			{
			  "title": "Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue.",
			  "content": "Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.",
			  "image": "https://www.ecestaticos.com/image/clipping/79776773aab795837282c7d4947abaf7/por-que-nos-parece-que-los-perros-sonrien-una-historia-de-30-000-anos.jpg",
			  "categoryId": 3
			}, {
			  "title": "Sed ante.",
			  "content": "Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo.",
			  "image": "http://c.files.bbci.co.uk/48DD/production/_107435681_perro1.jpg",
			  "categoryId": 2
			}, {
			  "title": "In quis justo.",
			  "content": "Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis.",
			  "image": "https://www.ecestaticos.com/image/clipping/79776773aab795837282c7d4947abaf7/por-que-nos-parece-que-los-perros-sonrien-una-historia-de-30-000-anos.jpg",
			  "categoryId": 0
			}, {
			  "title": "Fusce posuere felis sed lacus.",
			  "content": "Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam.",
			  "image": "https://www.ecestaticos.com/image/clipping/79776773aab795837282c7d4947abaf7/por-que-nos-parece-que-los-perros-sonrien-una-historia-de-30-000-anos.jpg",
			  "categoryId": 1
			}, {
			  "title": "Duis at velit eu est congue elementum.",
			  "content": "Duis bibendum. Morbi non quam nec dui luctus rutrum.",
			  "image": "https://www.ecestaticos.com/image/clipping/79776773aab795837282c7d4947abaf7/por-que-nos-parece-que-los-perros-sonrien-una-historia-de-30-000-anos.jpg",
			  "categoryId": 1
			}, {
			  "title": "Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue.",
			  "content": "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend.",
			  "image": "https://s1.eestatic.com/2021/04/25/curiosidades/mascotas/576453307_179177418_1024x576.jpg",
			  "categoryId": 1
			}, {
			  "title": "Vestibulum ac est lacinia nisi venenatis tristique.",
			  "content": "Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo.",
			  "image": "https://www.ecestaticos.com/image/clipping/79776773aab795837282c7d4947abaf7/por-que-nos-parece-que-los-perros-sonrien-una-historia-de-30-000-anos.jpg",
			  "categoryId": 1
			}, {
			  "title": "Nullam varius.",
			  "content": "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.",
			  "image": "https://s1.eestatic.com/2021/04/25/curiosidades/mascotas/576453307_179177418_1024x576.jpg",
			  "categoryId": 3
			}, {
			  "title": "Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante.",
			  "content": "Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
			  "image": "https://s1.eestatic.com/2021/04/25/curiosidades/mascotas/576453307_179177418_1024x576.jpg",
			  "categoryId": 0
			}, {
			  "title": "Ut at dolor quis odio consequat varius.",
			  "content": "Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat.",
			  "image": "https://www.ecestaticos.com/image/clipping/79776773aab795837282c7d4947abaf7/por-que-nos-parece-que-los-perros-sonrien-una-historia-de-30-000-anos.jpg",
			  "categoryId": 0
			}
		]);
		console.log("done!");
		process.exit(0);
	} catch (error) {
		console.log('Unable to connect to the database:');
		console.log(error.message);
		process.exit(1);
	}
}
reset();