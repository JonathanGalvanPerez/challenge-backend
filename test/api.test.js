const request = require('supertest');
const app = require('../app');
const sequelize = require('../sequelize');
const { Post, Category } = sequelize.models;

let post1 = {
	title: 'Un titulo',
	content: 'Un contenido',
	image: 'https://i.blogs.es/594843/chrome/450_1000.jpg',
	categoryId: '0'
};
let post2 = {
	title: 'Otro titulo',
	content: 'Otro contenido',
	image: 'https://i.blogs.es/594843/edge/450_1000.jpg',
	categoryId: '2'
};

let post3 = {
	title: 'Otro otro titulo',
	content: 'Otro otro contenido',
	image: 'https://i.blogs.es/594843/firefox/450_1000.jpg',
	categoryId: '3'
};
let categories = [];

beforeAll((done) => {
	sequelize.models.Category.findAll().then((result) => {
		categories = result.map(category => category.name);
	}).then(() => {
		return Post.destroy({ truncate: true });
	}).then(() => done());
});
afterAll((done) => {
	Post.destroy({ truncate: true }).then(() => {
		return sequelize.close();
	}).then(() => done());
});
beforeEach(() => {
	return Post.destroy({
		truncate: true
	});
});

describe('GET /posts', () => {
	it('respond with json containing a empty list of posts', done => {
		request(app)
			.get('/posts')
			.set('Accept', 'application/json')
			.expect('Content-Type', /json/)
			.expect(200, '[]', done);
	});
	it('respond with json containing one post', done => {
		Post.create(post1).then(() => {
			request(app)
				.get('/posts')
				.set('Accept', 'application/json')
				.expect('Content-Type', /json/)
				.expect(200)
				.then(res => {
					expect(res.body).toHaveLength(1);
					result = res.body[0];
					expect(result.title).toBe(post1.title);
					expect(result.image).toBe(post1.image);
					expect(result.category).toBe(categories[post1.categoryId]);
					return done();
				});
		});
	});
	it('respond with json containing two posts after add two post', done => {
		sequentialCreate([ post1, post2 ]).then(() => {
			request(app)
				.get('/posts')
				.set('Accept', 'application/json')
				.expect('Content-Type', /json/)
				.expect(200)
				.then(res => {
					expect(res.body).toHaveLength(2);
					result = res.body[0];
					expect(result.title).toBe(post2.title);
					expect(result.image).toBe(post2.image);
					expect(result.category).toBe(categories[post2.categoryId]);
					result = res.body[1];
					expect(result.title).toBe(post1.title);
					expect(result.image).toBe(post1.image);
					expect(result.category).toBe(categories[post1.categoryId]);
					return done();
				});
		});
	});
	it('respond with json containing one posts after delete one post', done => {
		sequentialCreate([ post1, post2 ]).then(() => {
			Post.destroy({ where: { title: "Otro titulo" } });
		}).then(() => {
			request(app)
				.get('/posts')
				.set('Accept', 'application/json')
				.expect('Content-Type', /json/)
				.expect(200)
				.then(res => {
					expect(res.body).toHaveLength(1);
					result = res.body[0];
					expect(result.title).toBe(post1.title);
					expect(result.image).toBe(post1.image);
					expect(result.category).toBe(categories[post1.categoryId]);
					return done();
				});
		});
	});
	it('respond with json containing two posts after update one post', done => {
		sequentialCreate([ post1, post2 ]).then(() => {
			return Post.update(post3, { where: { title: "Un titulo" } });
		}).then(() => {
			request(app)
				.get('/posts')
				.set('Accept', 'application/json')
				.expect('Content-Type', /json/)
				.expect(200)
				.then(res => {
					expect(res.body).toHaveLength(2);
					result = res.body[0];
					expect(result.title).toBe(post2.title);
					expect(result.image).toBe(post2.image);
					expect(result.category).toBe(categories[post2.categoryId]);
					result = res.body[1];
					expect(result.title).toBe(post3.title);
					expect(result.image).toBe(post3.image);
					expect(result.category).toBe(categories[post3.categoryId]);
					return done();
				});
		});
	});
});

describe('GET /posts/:id', () => {
	it('respond with json containing post properties', done => {
		Post.create(post1).then(() => {
			return Post.findAll().then((result) => result[0].id);
		}).then(id => {
			expect(id).not.toBeNull();
			request(app)
				.get('/posts/'+id)
				.set('Accept', 'application/json')
				.expect('Content-Type', /json/)
				.expect(200)
				.then(res => {
					let result = res.body;
					expect(result.id).toBe(id);
					expect(result.title).toBe(post1.title);
					expect(result.content).toBe(post1.content);
					expect(result.image).toBe(post1.image);
					expect(result.category).toBe(categories[post1.categoryId]);
					return done();
				});
		});
	});
	it('respond with error 404: Post id not found', done => {
		request(app)
			.get('/posts/1')
			.set('Accept', 'application/json')
			.expect('Content-Type', /text/)
			.expect(404, 'Error 404: Post id not found', done);
	});
	it('respond with error 404: Post id not found after delete a post', done => {
		let id = null;
		Post.create(post1).then(() => {
			return Post.findAll().then((result) => { id = result[0].id });
		}).then(() => {
			Post.destroy({ where: { title: 'Un titulo' } });
		}).then(() => {
			expect(id).not.toBeNull();
			request(app)
				.get('/posts/'+id)
				.set('Accept', 'application/json')
				.expect('Content-Type', /text/)
				.expect(404, 'Error 404: Post id not found', done);
		});
	});
	it('respond with json containing post properties after alter', done => {
		let id = null;
		Post.create(post1).then(() => {
			return Post.findAll().then((result) => { id = result[0].id });
		}).then(() => {
			Post.update(post3, { where: { title: 'Un titulo' } });
		}).then(() => {
			expect(id).not.toBeNull();
			request(app)
				.get('/posts/'+id)
				.set('Accept', 'application/json')
				.expect('Content-Type', /json/)
				.expect(200)
				.then(res => {
					let result = res.body;
					expect(result.id).toBe(id);
					expect(result.title).toBe(post3.title);
					expect(result.content).toBe(post3.content);
					expect(result.image).toBe(post3.image);
					expect(result.category).toBe(categories[post3.categoryId]);
					return done();
				})
		});
	});
});

describe('POST /posts', () => {
	it('respond with OK', done => {
		request(app)
			.post('/posts')
			.send(post1)
			.set('Accept', 'application/json')
			.expect('Content-Type', /text/)
			.expect(201, 'OK', done);
	});
	it('respond with error 400: Bad request', async (done) => {
		await request(app)
			.post('/posts')
			.send({})
			.set('Accept', 'application/json')
			.expect('Content-Type', /text/)
			.expect(400, 'Error 400: Bad request');
		await request(app)
			.post('/posts')
			.send({ id: '1', title: 'title', content: 'content', image: 'www.example.com/1.jpg', categoryId: '1' })
			.set('Accept', 'application/json')
			.expect('Content-Type', /text/)
			.expect(400, 'Error 400: Bad request');
		await request(app)
			.post('/posts')
			.send({ title: 'title', content: 'content', image: 'www.example.com/1.jpg', categoryId: '1', date: '1' })
			.set('Accept', 'application/json')
			.expect('Content-Type', /text/)
			.expect(400, 'Error 400: Bad request');
		await request(app)
			.post('/posts')
			.send({ content: 'content', image: 'www.example.com/1.jpg', categoryId: '1' })
			.set('Accept', 'application/json')
			.expect('Content-Type', /text/)
			.expect(400, 'Error 400: Bad request');
		await request(app)
			.post('/posts')
			.send({ title: 'title', image: 'www.example.com/1.jpg', categoryId: '1' })
			.set('Accept', 'application/json')
			.expect('Content-Type', /text/)
			.expect(400, 'Error 400: Bad request');
		await request(app)
			.post('/posts')
			.send({ title: 'title', content: 'content', categoryId: '1' })
			.set('Accept', 'application/json')
			.expect('Content-Type', /text/)
			.expect(400, 'Error 400: Bad request');
		await request(app)
			.post('/posts')
			.send({ title: 'title', content: 'content', image: 'www.example.com/1.jpg' })
			.set('Accept', 'application/json')
			.expect('Content-Type', /text/)
			.expect(400, 'Error 400: Bad request');
		done();
	});
	it('respond with error 400: Image url is not valid', done => {
		request(app)
			.post('/posts')
			.send({ title: 'title', content: 'content', image: 'www.example.com/1jpg', categoryId: '1' })
			.set('Accept', 'application/json')
			.expect('Content-Type', /text/)
			.expect(400, 'Error 400: Image url is not valid', done);
	});
});

describe('PATH /posts', () => {
	it('respond with OK', done => {
		Post.create(post1).then(() => {
			return Post.findAll().then((result) => result[0].id);
		}).then((id) => {
			request(app)
				.patch('/posts/'+id)
				.send({ title: 'title', content: 'content', image: 'www.example.com/1.jpg', categoryId: '1' })
				.set('Accept', 'application/json')
				.expect('Content-Type', /text/)
				.expect(200, 'OK', done);
		});
	});
	it('respond with error 404: Post id not found', done => {
		request(app)
			.patch('/posts/1')
			.send({ title: 'title', content: 'content', image: 'www.example.com/1.jpg', categoryId: '1' })
			.set('Accept', 'application/json')
			.expect('Content-Type', /text/)
			.expect(404, 'Error 404: Post id not found', done);
	});
	it('respond with error 400: Bad request', done => {
		request(app)
			.patch('/posts/1')
			.set('Accept', 'application/json')
			.expect('Content-Type', /text/)
			.expect(400, 'Error 400: Bad request', done);
	});
	it('respond with error 400: Image url is not valid', done => {
		request(app)
			.patch('/posts/1')
			.send({ title: 'title', content: 'content', image: 'www.example.com/1jpg', categoryId: '1' })
			.set('Accept', 'application/json')
			.expect('Content-Type', /text/)
			.expect(400, 'Error 400: image url is not valid', done);
	});
});

describe('DELETE /posts', () => {
	it('respond with OK', done => {
		Post.create(post1).then(() => {
			return Post.findAll().then((result) => result[0].id);
		}).then(id => {
			request(app)
				.delete('/posts/'+id)
				.set('Accept', 'application/json')
				.expect('Content-Type', /text/)
				.expect(200, 'OK', done);
		});
	});
	it('respond error 404: Post id not found', done => {
		request(app)
			.delete('/posts/1')
			.set('Accept', 'application/json')
			.expect('Content-Type', /text/)
			.expect(404, 'Error 404: Post id not found', done);
	});
});

async function sequentialCreate(posts) {
	for(var i=0; i<posts.length; i++) {
		await new Promise((resolve) => {
			setTimeout(() => {
				Post.create(posts[i]).then(() => resolve());
			}, 1000);
		});
	}
	return;
}