# Challenge Backend
API-REST para un blog desarrollado en Nodejs con el framework Express. Los datos son persistidos en una base de datos MySQL.
## Instalación
_Dirigirse a la carpeta **raiz**._

	npm install

_Iniciar servidor MySQL. Crear DB con el nombre **'challenge-backend'**_

_Iniciar tablas con el siguiente comando:_

	npm run setup

## Despliegue

	npm start

_Por defecto la API corre en **http://localhost:3000/**_