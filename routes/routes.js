const express = require('express');
const { models } = require('../sequelize');
const isImageUrl = require('is-image-url');
const Post = models.Post;
const Category = models.Category;

let router = express.Router();

router.get("/posts", (req, res) => {
	Post.findAll({
	  attributes: ['id', 'title', 'image', 'categoryId', 'createdAt'],
	  order: [
	  	['createdAt', 'DESC']
	  ],
	  include: {
	  	model: Category,
	  	as: 'category',
	  	attributes: ['name']
	  }
	}).then(result => {
		res.status(200).json(result.map(post => ({
			id: post.id,
			title: post.title,
			image: post.image,
			category: post.category.name,
			date: post.createdAt
		})));
	}).catch(error => {
		if(error)
			throw error;
	});
});

router.get("/posts/:id", (req, res) => {
	Post.findOne({
		where: {
			id: req.params.id
		},
		attributes: ['id', 'title', 'content', 'image', 'categoryId', 'createdAt'],
		include: {
	  	model: Category,
	  	as: 'category',
	  	attributes: ['name']
	  }
	}).then(post => {
		if(post)
			res.status(200).json({
				id: post.id,
				title: post.title,
				content: post.content,
				image: post.image,
				category: post.category.name,
				date: post.createdAt
			});
		else
			res.status(404).send('Error 404: Post id not found');
	}).catch(error => {
		if(error)
			throw error;
	});
});

router.post("/posts", (req, res) => {
	if(req.body.id || req.body.date || !(req.body.title && req.body.content && req.body.image && req.body.categoryId))
		res.status(400).send('Error 400: Bad request');
	else{
		if(!isImageUrl(req.body.image))
			res.status(400).send('Error 400: Image url is not valid');
		else
			Post.create({
				title: req.body.title,
				content: req.body.content,
				image: req.body.image,
				categoryId: req.body.categoryId,
			}).then(() => {
				res.status(201).send("OK");
			}).catch((error) => {
				if (error)
					throw error;
			});
	}
});

router.patch("/posts/:id", (req, res) => {
	if(Object.keys(req.body).length === 0)
		res.status(400).send("Error 400: Bad request");
	else if(req.body.image && !isImageUrl(req.body.image))
		res.status(400).send("Error 400: image url is not valid");
	else
		Post.update({
			title: req.body.title,
			content: req.body.content,
			image: req.body.image,
			categoryId: req.body.categoryId
		}, {
			where: {
				id: req.params.id
			}
		}).then((result) => {
			if (result[0] === 0)
				res.status(404).send('Error 404: Post id not found');
			else
				res.status(200).send("OK");
		}).catch(error => {
			throw error;
		});
});

router.delete("/posts/:id", (req, res) => {
	Post.destroy({
		where: {
			id: req.params.id
		}
	}).then((result) => {
		if (result === 0)
			res.status(404).send('Error 404: Post id not found');
		else
			res.status(200).send("OK");
	}).catch(error => {
		throw error;
	});
});

module.exports = router;