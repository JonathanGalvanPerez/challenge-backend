const { DataTypes, UUIDV4, NOW } = require('sequelize');

module.exports = (sequelize) => {
	sequelize.define('Post', {
		id: {
	    type: DataTypes.UUID,
	    defaultValue: UUIDV4,
	    primaryKey: true
		},
		title: {
			type: DataTypes.STRING,
	    allowNull: false
		},
		content: {
			type: DataTypes.TEXT,
	    allowNull: false
		},
		image: {
			type: DataTypes.STRING,
	    allowNull: false
		},
		categoryId: {
			type: DataTypes.UUID,
	    allowNull: false,
	    references: {
	    	model: 'categories',
	    	key: 'id'
	    }
		}
	}, {
		tableName: 'posts'
	});
};