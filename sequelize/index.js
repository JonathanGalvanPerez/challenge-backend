const { Sequelize } = require('sequelize');
const dbConfig = require('./db.config.js');

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  logging: false
});

require('./models/category.model')(sequelize);
require('./models/post.model')(sequelize);

const { Post, Category } = sequelize.models;
Category.hasMany(Post, { as: 'category', foreignKey: 'categoryId'});
Post.belongsTo(Category, { as: 'category', foreignKey: 'categoryId'});

module.exports = sequelize;