const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.json());
const PORT = 3000;

routes = require("./routes/routes.js");
app.use(routes);

app.listen(PORT, function(){
	console.log(`server is listening on port ${PORT}`);
});
module.exports = app;